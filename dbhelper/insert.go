package dbhelper

import (
	"errors"
	"fmt"
	"strings"
)

var (
	ErrUnableToInsert = errors.New("no fue posible insertar el registro")
)

/* ********************************************************************************************************** */
// dbInsert es un tipo de datos que permite la insersión de regitros en la BD
type dbInsert struct {
	*dbQuery
	model interface{}
}

// Into establece la tabla de destino del insert
func (m *dbInsert) Into(table string) *dbInsert {
	m.table = table
	return m
}

func (m *dbInsert) Columns(cols ...string) *dbInsert {
	cols, err := m.parseCols(cols...)
	if err != nil {
		m.err = err
		return m
	}

	m.cols = cols
	return m
}

// devuelve una representación en cadena de texto del sql insert
func (m *dbInsert) ToString() (string, error) {
	fields := []string{}
	values := []string{}
	for _, v := range m.cols {
		fields = append(fields, "`"+v+"`")
		values = append(values, ":"+v)
	}

	return fmt.Sprintf("INSERT INTO %s (%s) values (%s)",
			m.table,
			strings.Join(fields, ", "),
			strings.Join(values, ", ")),
		nil
}

// Ejecuta la instrucción Insert
// Esta función debe ser la última en llamarse, ya que es la que ejecuta el comando SQL
func (m *dbInsert) Exec() (int64, error) {
	sql, err := m.ToString()
	if err != nil {
		return 0, err
	}

	sql = m.db.Rebind(sql)

	log.Trace().Str("sql", sql).Interface("model", m.model).Msg("model.Insert")

	res, err := m.db.NamedExec(sql, m.model)
	if err != nil {
		return 0, err
	}

	affectedRows, err := res.RowsAffected()

	if err != nil {
		return 0, err
	}

	if affectedRows == 0 {
		log.Warn().Int64("affectedRows", affectedRows).Msg("No se insertó ningún elemento")
		return 0, ErrUnableToInsert
	}

	lastID, err := res.LastInsertId()

	if err != nil {
		return 0, err
	}

	return lastID, nil

}
