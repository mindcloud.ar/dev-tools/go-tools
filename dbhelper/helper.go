package dbhelper

import (
	"fmt"
	"strings"
)

// SanitizeFields Sanitiza los nombres de campos dependiendo del tipo de base de datos especificado
func SanitizeFields(dialect string, fields []string) []string {
	var result []string
	var quotationMarkLeft string
	var quotationMarkRight string

	switch dialect {
	case "mysql":
		quotationMarkLeft = "`"
		quotationMarkRight = "`"
	case "mssql":
		quotationMarkLeft = "["
		quotationMarkRight = "]"
	default:
		quotationMarkLeft = `"`
		quotationMarkRight = `"`
	}
	// invalid := regexp.MustCompile(fmt.Sprintf("[%s%s]", quotationMarkLeft, quotationMarkRight))

	for _, f := range fields {
		f = strings.ReplaceAll(f, quotationMarkLeft, "")
		f = strings.ReplaceAll(f, quotationMarkRight, "")
		result = append(result, fmt.Sprintf("%s%s%s", quotationMarkLeft, f, quotationMarkRight))
	}

	return result

}
