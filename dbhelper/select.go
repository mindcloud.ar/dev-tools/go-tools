package dbhelper

import (
	"errors"
	"fmt"
	"strings"
)

// DBSelect es una estructura que permite serializar la ejecución de Select
type DBSelect struct {
	*dbQuery
	sort    []string
	limit   int64
	groupby []string
}

// Into establece el destino del resultado del select
func (m *DBSelect) Into(dest interface{}) *DBSelect {
	m.dest = dest
	return m
}

func (m *DBSelect) From(table string) *DBSelect {
	if err := m.validateTable(table); err != nil {
		m.err = err
	}
	m.table = table
	return m
}

// OrderBy recibe una lista de campos por los cuales se debe realizar el OrderBy
// si alguno de los campos contiene un prefijo "-" se ordena descendente por ese campo
func (m *DBSelect) OrderBy(sort []string) *DBSelect {

	sort = m.parseOrderBy(sort)

	m.sort = sort
	return m
}

/*
// LeftJoin ...
func (m *dbSelect) LeftJoin(table string) *dbSelect {
	if err := m.validateTable(table); err != nil {
		m.err = fmt.Errorf("LeftJoin: %s", err)
	}

	m.leftJoin = table
	return m
}

// On ...
func (m *dbSelect) On(cond string) *dbSelect {
	m.on = cond
	return m
}
*/
// GroupBy ...
func (m *DBSelect) GroupBy(cols ...string) *DBSelect {
	cols, err := m.parseCols(cols...)
	if err != nil {
		m.err = err
	}
	m.cols = cols

	m.groupby = cols
	return m
}

// Limit Establece un límite a la cantidad de registros devueltos por la BD
// Por conveniencia, limit es un string, que puede ser vacío (sin limit)
func (m *DBSelect) Limit(limit int64) *DBSelect {

	m.limit = int64(limit)
	return m
}

// Offset establece el registro de inicio para el resultado de la consulta a BD
// es un string que puede ser vacío (se ignora)
func (m *DBSelect) Offset(offset int64) *DBSelect {

	m.offset = offset
	return m
}

// Where agrega una condición a la búsqueda
// la condición es una condición sql válida para la sentencia where, por ejemplo `name = ?`
// los valores son valores a asociar a los placeholders ('?') de la sentencia anterior
// se puede llamar a esta función múltiples veces
// ejemplo: Select(...).Where('name = ?', nombre).Where('edad > ?' and edad < ?, edadDesde, edadHasta)
func (m *DBSelect) Where(query string, values ...interface{}) *DBSelect {
	m.where = append(m.where, query)
	m.args = append(m.args, values...)
	return m
}

// HasErrors indica si hubo algun error en la cadena de funciones
func (m *DBSelect) HasErrors() bool {
	return m.err != nil
}

// Err devuelve el último error
func (m *DBSelect) Err() error {
	return m.err
}

// ToString devuelve un string con la sentencia Sql del query
func (m *DBSelect) ToString() (string, error) {
	if m.err != nil {
		return "", m.err
	}

	var limit, offset, orderBy, where, leftjoin, groupby string

	if m.limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", m.limit)
		if m.offset > 0 {
			offset = fmt.Sprintf(" OFFSET %d", m.offset)
		}
	}

	if m.leftJoin != "" {
		leftjoin = " LEFT JOIN " + m.leftJoin
		if m.on != "" {
			leftjoin += " ON (" + m.on + ")"
		}
	}

	if len(m.sort) > 0 {
		orderBy = " ORDER BY " + strings.Join(m.sort, ", ")
	}

	if len(m.groupby) > 0 {
		groupby = " GROUP BY " + strings.Join(m.groupby, ", ")
	}

	if len(m.where) > 0 {
		where = " WHERE " + strings.Join(m.where, " AND ")
	}

	cols := []string{}
	for _, c := range m.cols {
		c = strings.TrimSpace(c)
		if c != "*" {
			c = fmt.Sprintf("`%s`", c)
		}
		cols = append(cols, c)
	}

	sql := "select " + strings.Join(cols, ", ") + " from " + m.table + leftjoin + where + groupby + orderBy + limit + offset
	sql = m.db.Rebind(sql)

	log.Trace().Str("sql", sql).Msg("")
	return sql, nil

}

// Args devuelve la lista de valores cargados en las clausulas where
// estos valores son los valores a bindear en el select
func (m *DBSelect) Args() []interface{} {
	return m.args
}

func (m *DBSelect) Exec() error {
	if m.dest == nil {
		return errors.New("no se especificó el destino del select")
	}

	sql, err := m.ToString()
	if err != nil {
		return err
	}

	if err := m.db.Select(m.dest, sql, m.args...); err != nil {
		log.Error().Err(err).Msg("error en select")
		m.err = err
		return err
	}

	m.err = nil
	return nil

}

/*

// Delete elimina un registro de la BD
// model es una estructura que implementa Modeler
// id es una lista de valores para las claves primarias del modelo
func Delete(model Modeler, id ...interface{}) error {

	var err error
	sql := "delete from " + model.GetName() + " where "

	pk := model.GetPrimaryKeys()
	var s []string
	for i := range pk {
		s = append(s, pk[i]+"= ?")
	}
	sql += strings.Join(s, " AND ")

	log.Debug().Str("sql", sql).Msg("model.Delete")

	res, err := DB.Exec(sql, id...)
	if err != nil {
		return err
	}

	affectedRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affectedRows == 0 {
		log.Warn().Interface("id", id).Int64("affectedRows", affectedRows).Msg("No se eliminó ningún elemento")
		return errors.New("No se encontró el recurso específicado")
	}
	return err
}

*/

/*

// Update Inicia una sentencia Insert en la BD
func Update(table string) *dbUpdate {
	return &dbUpdate{
		table: table,
	}
}

// dbUpdate es un tipo de datos que permite la insersión de regitros en la BD
type dbUpdate struct {
	table  string
	cols   []string
	values interface{}
}

// Set campos a modificar
func (m *dbUpdate) Set(cols ...string) *dbUpdate {
	m.cols = cols
	return m
}

// Values valores a actualizar
func (m *dbUpdate) Values(values ...interface{}) *dbUpdate {
	m.values = values
	return m
}

// Where ejecuta el Update sobre las claves primarias especificadas
// el primer parámetro es la lista de campos que son claves primarias
// El segundo campo es la lista de valores para esas claves primarias
// la función utiliza estos valores para armar el string "WHERE <clave_1> = ? AND <clave_"> = ? ...."
// Esta función debe ser la última en llamarse, ya que es la que ejecuta el comando SQL
func (m *dbUpdate) Where(pks []string, v []interface{}) error {
	// var binds []string
	mValues := m.values.([]interface{})
	var values []interface{}
	var cols []string
	for i := range mValues {
		cols = append(cols, m.cols[i]+" = ?")
		values = append(values, mValues[i])
	}

	var cond []string
	for i := range pks {
		cond = append(cond, pks[i]+" = ?")
	}

	values = append(values, v...)

	sql := "UPDATE " + m.table + " SET " + strings.Join(cols, ",") + " WHERE " + strings.Join(cond, " AND ")

	log.Debug().Str("sql", sql).Interface("binds", values).Msg("model.Update")

	res, err := DB.Exec(sql, values...)
	if err != nil {
		return err
	}

	affectedRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affectedRows == 0 {
		log.Warn().Int64("affectedRows", affectedRows).Msg("No se actualizó ningún elemento")
	}

	return nil
}
*/
