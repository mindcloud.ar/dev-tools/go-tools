package dbhelper

import (
	"errors"
	"regexp"

	"github.com/go-sqlx/sqlx"
)

var (
	ErrInvalidTableName = errors.New("el nombre de la tabla es inválido")
	ErrInvalidFieldName = errors.New("uno de los campos especificados no es válido")
)

type dbQuery struct {
	db       *sqlx.DB
	dest     interface{}
	cols     []string
	table    string
	leftJoin string
	on       string
	where    []string
	args     []interface{}
	offset   int64
	err      error
}

// Devuelve un nuevo query que se ejecutará en la BD indicada
func NewQuery(db *sqlx.DB) *dbQuery {
	return &dbQuery{
		db: db,
	}
}

// Select Inicia la creación de una sentencia formada por cadena de funciones
func (m *dbQuery) Select(cols ...string) *DBSelect {

	cols, err := m.parseCols(cols...)
	if err != nil {
		m.err = err
	}
	m.cols = cols

	return &DBSelect{
		dbQuery: m,
		limit:   1000,
	}
}

// Select Inicia la creación de una sentencia formada por cadena de funciones
func (m *dbQuery) Get(cols ...string) *dbGet {

	cols, err := m.parseCols(cols...)
	if err != nil {
		m.err = err
	}
	m.cols = cols

	return &dbGet{
		dbQuery: m,
	}
}

// TODO: implementar transacciones para insert múltiples
// Insert Inicia una sentencia Insert en la BD
// parametro model contiene los datos a almacenar
func (m *dbQuery) Insert(model interface{}) *dbInsert {
	return &dbInsert{
		dbQuery: m,
		model:   model,
	}
}

func (m *dbQuery) validateTable(table string) error {
	re := regexp.MustCompile(`^[\w\W]+$`)
	if !re.MatchString(table) {
		return ErrInvalidTableName
	}
	return nil
}

func (m *dbQuery) parseOrderBy(sort []string) []string {
	//_sort contiene los campos válidos para sort
	var _sort []string
	re := regexp.MustCompile(`^[0-9a-zA-Z_-]+\s*(DESC)*$`)

	for i := range sort {
		prefijo := string(sort[i][0])
		if !re.MatchString(sort[i]) {
			log.Warn().Str("campo", sort[i]).Msg("Campo Inválido en Sort")
			continue
		}
		if prefijo == "-" {
			sort[i] = sort[i][1:] + " DESC"
		}
		_sort = append(_sort, sort[i])
	}

	return _sort
}

func (m *dbQuery) parseCols(cols ...string) ([]string, error) {
	re := regexp.MustCompile(`^[\w\W]+$`)
	var _cols []string

	for _, v := range cols {
		if !re.MatchString(v) {
			return nil, ErrInvalidFieldName
		}
		_cols = append(_cols, v)
	}

	return _cols, nil
}
