package dbhelper

import (
	// "github.com/rs/zerolog/log"
	// "errors"
	"errors"
	"fmt"
	"strings"
	// "fmt"
	// "database/sql"
	// "github.com/go-sqlx/sqlx"
)

// dbGet es una estructura que permite serializar la ejecución de Select
type dbGet struct {
	*dbQuery
	sort    []string
	limit   int64
	groupby []string
}

// Into establece el destino del resultado del get
func (m *dbGet) Into(dest interface{}) *dbGet {
	m.dest = dest
	return m
}

func (m *dbGet) From(table string) *dbGet {
	if err := m.validateTable(table); err != nil {
		m.err = err
	}
	m.table = table
	return m
}

/*
// OrderBy recibe una lista de campos por los cuales se debe realizar el OrderBy
// si alguno de los campos contiene un prefijo "-" se ordena descendente por ese campo
func (m *dbGet) OrderBy(sort []string) *dbGet {

	sort = m.parseOrderBy(sort)

	m.sort = sort
	return m
}
*/

/*
// LeftJoin ...
func (m *dbGet) LeftJoin(table string) *dbGet {
	if err := m.validateTable(table); err != nil {
		m.err = fmt.Errorf("LeftJoin: %s", err)
	}

	m.leftJoin = table
	return m
}

// On ...
func (m *dbGet) On(cond string) *dbGet {
	m.on = cond
	return m
}
*/

// Where agrega una condición a la búsqueda
// la condición es una condición sql válida para la sentencia where, por ejemplo `name = ?`
// los valores son valores a asociar a los placeholders ('?') de la sentencia anterior
// se puede llamar a esta función múltiples veces
// ejemplo: Get(...).Where('name = ?', nombre).Where('edad > ?' and edad < ?, edadDesde, edadHasta)
func (m *dbGet) Where(query string, values ...interface{}) *dbGet {
	m.where = append(m.where, query)
	m.args = append(m.args, values...)
	return m
}

// WithKey establece la búsqueda por el id especificado
func (m *dbGet) WithKey(identifier string, value interface{}) *dbGet {
	ids, err := m.parseCols(identifier)
	if err != nil {
		m.err = err
		return m
	}

	m.where = append(m.where, fmt.Sprintf("%s = ?", ids[0]))
	m.args = append(m.args, value)
	return m
}

// HasErrors indica si hubo algun error en la cadena de funciones
func (m *dbGet) HasErrors() bool {
	return m.err != nil
}

// Err devuelve el último error
func (m *dbGet) Err() error {
	return m.err
}

// ToString devuelve un string con la sentencia Sql del query
func (m *dbGet) ToString() (string, error) {
	if m.err != nil {
		return "", m.err
	}

	var limit, offset, orderBy, where, leftjoin, groupby string

	if m.limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", m.limit)
		if m.offset > 0 {
			offset = fmt.Sprintf(" OFFSET %d", m.offset)
		}
	}

	if m.leftJoin != "" {
		leftjoin = " LEFT JOIN " + m.leftJoin
		if m.on != "" {
			leftjoin += " ON (" + m.on + ")"
		}
	}

	if len(m.sort) > 0 {
		orderBy = " ORDER BY " + strings.Join(m.sort, ", ")
	}

	if len(m.groupby) > 0 {
		groupby = " GROUP BY " + strings.Join(m.groupby, ", ")
	}

	if len(m.where) > 0 {
		where = " WHERE " + strings.Join(m.where, " AND ")
	}

	cols := []string{}
	for _, c := range m.cols {
		c = strings.TrimSpace(c)
		if c != "*" {
			c = fmt.Sprintf("`%s`", c)
		}
		cols = append(cols, c)
	}

	sql := "select " + strings.Join(cols, ", ") + " from " + m.table + leftjoin + where + groupby + orderBy + limit + offset
	sql = m.db.Rebind(sql)

	log.Trace().Str("sql", sql).Msg("")
	return sql, nil

}

// Args devuelve la lista de valores cargados en las clausulas where
// estos valores son los valores a bindear en el select
func (m *dbGet) Args() []interface{} {
	return m.args
}

func (m *dbGet) Exec() error {
	if m.dest == nil {
		return errors.New("no se especificó el destino del select")
	}

	sql, err := m.ToString()
	if err != nil {
		return err
	}

	if err := m.db.Get(m.dest, sql, m.args...); err != nil {
		log.Error().Err(err).Msg("error en get")
		m.err = err
		return err
	}

	m.err = nil
	return nil

}
