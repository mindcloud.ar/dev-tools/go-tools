package dbhelper

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/go-sqlx/sqlx"
	_log "github.com/rs/zerolog/log"

	// migracion
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database"
	mysqlMigrationDriver "github.com/golang-migrate/migrate/database/mysql"

	//importando source file
	_ "github.com/golang-migrate/migrate/source/file"
)

var log = _log.With().Str("pkg", "db").Logger()

// NewConnection devuelve una nueva instancia de BD
func NewConnection(dbType, dbHost, dbPort, dbName, user, password, migrateFolder string) (*sqlx.DB, error) {

	if migrateFolder == "" {
		migrateFolder = "default"
	}

	if err := validateParams(dbType, dbHost, dbPort, dbName, user, password); err != nil {
		return nil, err
	}

	connStr, err := getConnStr(dbType, dbHost, dbPort, dbName, user, password)
	if err != nil {
		return nil, err
	}

	log = log.With().Str("ctx", "Init").Logger()

	log.Trace().Str("Host", dbHost).Str("Port", dbPort).Str("Schema", dbName).Msg("Conectando con la Base de Datos")

	db, err := sqlx.Connect(dbType, connStr)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	if err := doMigrate(migrateFolder, dbType, connStr); err != nil {
		return nil, fmt.Errorf("error en migración: '%s'", err)
	}

	return db, nil
}

func doMigrate(subfolder string, dbType string, connStr string) error {
	//migración de BD
	log.Debug().Msg("Iniciando Migración")
	var conn *sql.DB
	var m *migrate.Migrate
	var driver database.Driver
	var err error

	switch dbType {
	case "mysql":
		conn, _ = sql.Open(dbType, connStr+"?multiStatements=true")
		driver, _ = mysqlMigrationDriver.WithInstance(conn, &mysqlMigrationDriver.Config{})
	default:
		return fmt.Errorf("tipo de base de datos '%s' no soportado", dbType)
	}

	if err != nil {
		return err
	}

	m, err = migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file://schema/migrate/%s", subfolder),
		dbType,
		driver,
	)

	if err != nil {
		return err
	}

	if err := m.Up(); err != nil && err.Error() != "no change" {
		return err
	}
	log.Debug().Msg("Fin de  Migración")

	return nil
}

func validateParams(dbType, dbHost, dbPort, dbName, user, password string) error {
	switch dbType {
	case "mysql":
	default:
		return fmt.Errorf("tipo de BD '%s' no soportado", dbType)
	}

	if dbHost == "" {
		return errors.New("hostname no definido")
	}

	if dbPort == "" {
		return errors.New("puerto de BD  no definido")
	}

	if dbName == "" {
		return errors.New("esquema  no definido")
	}

	if user == "" {
		return errors.New("usuario  no definido")
	}

	if password == "" {
		return errors.New("password  no definido")
	}

	return nil
}

func getConnStr(dbType, dbHost, dbPort, dbName, user, password string) (string, error) {
	switch dbType {
	case "mysql":
		return fmt.Sprintf("%s:%s@(%s:%s)/%s", user, password, dbHost, dbPort, dbName), nil
	default:
		return "", fmt.Errorf("tipo de BD '%s' no soportado", dbType)
	}
}
