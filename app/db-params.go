package app

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
)

//DBParams parámetros de conexión a la BD
type DBParams struct {
	Dsn        string
	Type       string
	Host       string
	Schema     string
	Port       int
	Username   string
	Secret     string `yaml:"secret,omitempty"`
	SecretFile string `yaml:"secret-file,omitempty"`
}

//GetConnectionString devuelve un connection string compatible con sqlx
func (p *DBParams) GetConnectionString() (string, error) {
	if p.Dsn != "" {
		return p.Dsn, nil
	}

	var connStr string

	var secret string
	if p.SecretFile != "" {
		data, err := ioutil.ReadFile(p.SecretFile)
		if err != nil {
			return "", err
		}
		secret = strings.TrimSuffix(string(data), "\n")

	} else {
		secret = p.Secret
	}

	if p.Host == "" {
		return "", errors.New("Hostname de la BD no especificado")
	}
	if p.Username == "" {
		return "", errors.New("Username no especificado")
	}
	if secret == "" {
		return "", errors.New("Contraseña no especificada")
	}
	if p.Schema == "" {
		return "", errors.New("Esquema de BD no esepecificado")
	}

	var host = p.Host
	if p.Port != 0 {
		host = fmt.Sprintf("%s:%d", p.Host, p.Port)
	}

	switch p.Type {
	case "sqlserver":
		connStr = fmt.Sprintf("sqlserver://%s:%s@%s?database=%s", p.Username, secret, host, p.Schema)
	case "mysql":
		connStr = fmt.Sprintf("%s:%s@tcp(%s)/%s", p.Username, secret, host, p.Schema)
	default:
		connStr = fmt.Sprintf("%s:%s@%s/%s", p.Username, secret, host, p.Schema)
	}

	return connStr, nil

}
