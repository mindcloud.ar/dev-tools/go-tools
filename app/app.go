// Package app paquete que contiene la configuración y herramientas comunes a todos los proyetos
package app

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"strconv"
)

func init() {
	//configuro zerolog
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
	if err != nil {
		logLevel = 3
	}
	zerologLevel := zerolog.Level(logLevel)
	zerolog.SetGlobalLevel(zerologLevel)
	log.Logger = log.With().Caller().Logger()
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

}

var DevMode = false
