// Package middlewares project middlewares.go
package middleware

import (
	"context"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
	_log "github.com/rs/zerolog/log"

	"gitlab.com/mindcloud.ar/dev-tools/go-tools/render"
)

var log = _log.With().Str("pkg", "middleware").Logger()

// ctxKeyType es un nuevo tipo utilizado para generar keys para los contextos
type ctxKeyType string

const requestParamsCtxKey ctxKeyType = "requestParams"

// ParseParamsCtx es un middleware que obtiene los parámetros de la solicitud y los almacena en el contexto
func ParseParamsCtx(next http.Handler) http.Handler {
	log = log.With().Str("ctx", "ParseParamsCtx").Logger()
	fn := func(w http.ResponseWriter, r *http.Request) {
		params := GetContextParams(r)
		ctx := r.Context()
		var err error

		urlQuery := r.URL.Query()

		params.Search = parseSearch(urlQuery.Get("q"))

		offset := urlQuery.Get("offset")
		if offset == "" {
			offset = urlQuery.Get("Offset")
		}
		offset = strings.TrimSpace(offset)

		limit := urlQuery.Get("limit")
		if limit == "" {
			limit = urlQuery.Get("Limit")
		}
		limit = strings.TrimSpace(limit)

		params.Limit = limitParam{}
		params.Limit.Offset, _ = strconv.ParseInt(offset, 10, 64)
		params.Limit.Count, err = strconv.ParseInt(limit, 10, 64)
		if err != nil {
			params.Limit.Count = -1
		}

		sl := sortList{}
		sl.Parse(urlQuery["sort"])
		// log.Trace().Interface("ccc", urlQuery["sort"]).Err(err).Msg("coso de sort")

		params.Sort = sl

		delete(urlQuery, "limit")
		delete(urlQuery, "offset")
		delete(urlQuery, "q")
		delete(urlQuery, "sort")

		// log.Debug().Interface("content-type", r.Header.Get("Content-Type")).Msg("")
		contentType := r.Header.Get("Content-Type")
		if strings.HasPrefix(contentType, "multipart/form-data;") {
			if err := r.ParseMultipartForm(10 << 20); err == nil {
				if r.MultipartForm != nil && r.MultipartForm.File != nil {

					for _, fls := range r.MultipartForm.File {
						for _, file := range fls {
							f, er := file.Open()
							if er != nil {
								log.Error().Err(er).Interface("object", file.Filename).Msg("Error al abrir archivo")
								continue
							}
							defer f.Close()

							tmpFile := FileData{}
							// tmpFile.Header = file.Header
							tmpFile.Filename = file.Filename
							tmpFile.Size = file.Size
							tmpFile.ContentType = file.Header.Get("Content-Type")
							tmpFile.File = f

							params.Files = append(params.Files, tmpFile)

						}
					}
				}
			}

		}
		//parseo el body
		r.Body = http.MaxBytesReader(w, r.Body, 20971520)
		b, err := io.ReadAll(r.Body)
		if err != nil {
			render.Render(w, r, render.NewGenericErrorResponse(err))
			// app.Fail(w, err.Error())
			return

		}

		params.RequestBody = b

		params.QueryParams = urlQuery
		params.Chi = chi.RouteContext(ctx)

		ctx = context.WithValue(r.Context(), requestParamsCtxKey, params)
		log.Trace().Interface("data", params).Msg("Parseando Parámetros para URL " + r.URL.String())

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

type sortList []string

// Parse
//
//	parsea el parámetro sort recibido, armando una lista de los campos por los que
//	se debe ordernar
func (sl *sortList) Parse(sort []string) error {

	re := regexp.MustCompile(`^[\w\W]+$`)
	// var lista sortList = sortList{}
	log.Trace().Interface("sort", sort).Msg("parseando sort")
	for _, s := range sort {
		//limpio los espacios
		s = strings.TrimSpace(s)

		//por defecto ordeno ascendente
		sufix := ""

		//limpio los prefijos
		if strings.HasPrefix(s, "-") {
			s = strings.TrimLeft(s, "-")
			sufix = " DESC"
		} else if strings.HasPrefix(s, "+") {
			s = strings.TrimLeft(s, "+")
		}

		//vuelvo a limpiar los espacios
		s = strings.TrimSpace(s)

		//si no es un campo válido, lo ignoro
		if !re.MatchString(s) {
			continue
		}

		*sl = append(*sl, s+sufix)

	}
	return nil
}

type limitParam struct {
	Offset int64
	Count  int64
}

func (l *limitParam) Sql(dialect string, includeSentece bool) (string, error) {
	sql := ""
	switch dialect {
	case "mysql":
		if l.Count >= 0 {
			if includeSentece {
				sql += " LIMIT "
			}
			if l.Offset > 0 {
				sql += fmt.Sprintf("%v,%v ", l.Offset, l.Count)
			} else {
				sql += fmt.Sprintf("%v ", l.Count)
			}
		}

	default:
		return sql, errors.New("BD no soportada")
	}
	return sql, nil
}

// Estructura que contiene los parámetros de la solicitud
type requestParams struct {
	//Contiene la lista de campos para el sort del resultado
	Sort sortList

	//Contiene el texto para realización de search (parámetro Get q)
	Search string

	//Contiene los parámetros de offset y limit del request
	Limit limitParam

	//contiene los datos parseados del  body recibido via POST/PUT
	RequestBody []byte

	//Realiza la búsqueda de un parámetro Get recibido en el request
	QueryParams url.Values

	//Referencia al Contexto del ruteador chi
	Chi *chi.Context

	//Contiene la información del usuario obtenida del token jwt
	// UserInfo userInfo

	// Permisos permiso

	//Contiene los archivos y la informacion de los mismos del form-data
	Files []FileData
}

type FileData struct {
	Filename    string
	File        multipart.File
	Size        int64
	ContentType string
}

// parseSearch
//
//	Parsea el parámetro de búsqueda
func parseSearch(param string) string {

	//TODO: sanitizar

	return param
}

// GetContextParams devuelve los parámetros del request actual
func GetContextParams(r *http.Request) *requestParams {
	ctx := r.Context()
	v := ctx.Value(requestParamsCtxKey)
	if v == nil {
		return &requestParams{}
	}
	return v.(*requestParams)
}
