// Package handler contiene una cantidad de handlers últiles
package handler

import (
	"net/http"

	_log "github.com/rs/zerolog/log"
)

var log = _log.With().Str("pkg", "handler").Logger()

func Ping(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("pong")
	w.Write([]byte(`pong`))
}
